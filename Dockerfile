FROM maven:3.6.3-jdk-11
ARG JAR_FILE=target/*.jar
ENV JAVA_OPTS=''
COPY ${JAR_FILE} /opt/eurekaserver-0.0.1-SNAPSHOT.jar
ENTRYPOINT ["sh", "-c", "java $JAVA_OPTS -jar /opt/eurekaserver-0.0.1-SNAPSHOT.jar"]
EXPOSE 8761
